FROM node:lts-alpine as build-stage

WORKDIR /app
ADD package.json .
ADD yarn.lock .
RUN rm -rf node_modules
RUN yarn install
COPY . .
ENV PATH="/app/node_modules/.bin:${PATH}"
RUN NODE_ENV="production" yarn build

FROM nginx:stable-alpine as production-stage
ENV NODE_ENV="production"
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
ADD ./nginx-default.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
