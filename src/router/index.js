import { createRouter, createWebHistory } from "vue-router";
import Project from "@/views/Project";
import Sandbox from "@/views/Sandbox";
import Storage from "@/views/Storage";
import Vault from "@/views/Vault";

const routes = [
  {
    path: "/",
    name: "Project",
    component: Project,
  },
  {
    path: "/sandbox",
    name: "Sandbox",
    component: Sandbox,
  },
  {
    path: "/storage/:kex?/:table?/:action?",
    name: "Storage",
    component: Storage,
  },
  {
    path: "/vault/:type?/:item?",
    name: "Vault",
    component: Vault,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  base: process.env.NODE_ENV === "production" ? "/flow/" : null,
});

export default router;
