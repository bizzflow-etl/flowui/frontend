import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import StorageSelector from "@/components/StorageSelector";
import TablesListing from "@/components/TablesListing";
import ItemSelector from "@/components/ItemSelector";
import ApiData from "@/components/ApiData";
import ModalDialog from "@/components/ModalDialog";
import SectionContainer from "@/components/SectionContainer";
import DropdownMenu from "@/components/DropdownMenu";
import ProgressSpinner from "@/components/ProgressSpinner";

const app = createApp(App);

app.config.globalProperties.api_url =
  process.env.NODE_ENV === "production"
    ? "{origin}/flow/api"
    : "http://localhost:9090";

app.config.globalProperties.base_url =
  process.env.NODE_ENV === "production" ? "/flow/" : "/";

app.config.globalProperties.synchronousOperationTimeout = 20;

app
  .use(router)
  .component("ItemSelector", ItemSelector)
  .component("ApiData", ApiData)
  .component("StorageSelector", StorageSelector)
  .component("TablesListing", TablesListing)
  .component("ModalDialog", ModalDialog)
  .component("SectionContainer", SectionContainer)
  .component("DropdownMenu", DropdownMenu)
  .component("ProgressSpinner", ProgressSpinner)
  .mixin({
    methods: {
      notify(text, title, type) {
        // console.log({ text });
        // console.log(typeof text);
        if (typeof text === "object") {
          if (Object.prototype.hasOwnProperty.call(text, "response")) {
            text = `${text.message}<br />${text.response.data?.detail}`;
          }
        }
        this.$root.$refs.notificationArea.createNotification(text, title, type);
      },
      pushState(base, children, title) {
        // Push state into history in the format of '{base}/{child1}/{child2}/.../{childn}
        let suffix = children.join("/");
        let url = `${app.config.globalProperties.base_url}${base}/${suffix}`;
        history.pushState({}, title, url);
      },
    },
    computed: {
      apiUrl() {
        return `${this.api_url}`.replace("{origin}", window.location.origin);
      },
    },
  })
  .mount("#app");
