const vaultItemConfig = {
  credentials: {
    icon: "fas fa-key",
    show: false,
  },
  storage: {
    icon: "fas fa-database",
    show: false,
  },
  sandbox: {
    icon: "fas fa-box",
    show: true,
  },
  datamart: {
    icon: "fas fa-upload",
    show: true,
  },
  default: {
    icon: "fas fa-lock",
    show: false,
  },
};

module.exports.getVaultItemConfig = (itemType) => {
  if (Object.prototype.hasOwnProperty.call(vaultItemConfig, itemType)) {
    return vaultItemConfig[itemType];
  }
  return vaultItemConfig.default;
};
